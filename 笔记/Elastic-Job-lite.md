## Elastic-Job
### 综述
* 当当⽹开源的⼀个分布式调度解决⽅案
* 基于Quartz⼆次开发的
* 由两个相互独⽴的⼦项⽬Elastic-Job-Lite和Elastic-Job-Cloud组成
* Elastic-Job-Lite，轻量级⽆中⼼化解决⽅案，使⽤Jar包的形式提供分布式任务的协调服务
* Elastic-Job-Cloud⼦项⽬需要结合Mesos以及Docker在云环境下使⽤
* 控制面板，执行情况？
### 功能
* 分布式调度协调：在分布式环境中，任务能够按指定的调度策略执⾏，并且能够避免同⼀任务多实例重复执⾏
* 丰富的调度策略：基于成熟的定时任务作业框架Quartz cron表达式执⾏定时任务
* 弹性扩容缩容：当集群中增加某⼀个实例，它应当也能够被选举并执⾏任务；当集群减少⼀个实例时，它所执⾏的任务能被转移到别的实例来执⾏。
* 失效转移：某实例在任务执⾏失败后，会被转移到其他实例执⾏
* 错过执⾏作业重触发：若因某种原因导致作业错过执⾏，⾃动记录错过执⾏的作业，并在上次作业完成后⾃动触发。
* ⽀持并⾏调度：⽀持任务分⽚，任务分⽚是指将⼀个任务分为多个⼩任务项在多个实例同时执⾏。
* 作业分⽚⼀致性：当任务被分⽚后，保证同⼀分⽚在分布式环境中仅⼀个执⾏实例
### Elastic-Job-Lite特性
* 轻量级和去中⼼化
    ![avatar](轻量级和去中⼼化.png)
* 任务分片与缩容扩容
    ![avatar](任务分片.png)
    1. 大数据量的处理，自然而然需要并行处理，即分片。可以是多台服务器或者一台服务器多个线程，或者两者组合。
    2. 弹性缩扩容，采用ZK注册中心，当有新服务上下线，即重新分片，完成弹性缩扩容和失效转移
### 监控elastic-job-lite-console
