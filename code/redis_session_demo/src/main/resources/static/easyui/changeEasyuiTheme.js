function changeThemeFun(themeName) {/* 更换主题 */
      var $easyuiTheme = $('#easyuiTheme');
      var $customizeDemo = $('#customizeDemo');
      //var $customizeTheme = $('#customizeDemo');
      var url = $easyuiTheme.attr('href');
      var customizeurl = url.substring(0, url.indexOf('themes'))+'demo.css';
      var href = url.substring(0, url.indexOf('themes')) + 'themes/' + themeName + '/easyui.css';
      if(themeName=='hrgj'){
    	  customizeurl=url.substring(0, url.indexOf('themes'))+'hrgj.css';
    	  href=url.substring(0, url.indexOf('themes')) + 'themes/cupertino/easyui.css';
      }
      $customizeDemo.attr('href', customizeurl);
      $easyuiTheme.attr('href', href);
      var $iframe = $('iframe');
      if ($iframe.length > 0) {
          for ( var i = 0; i < $iframe.length; i++) {
             var ifr = $iframe[i];
             $(ifr).contents().find('#easyuiTheme').attr('href', href);
             $(ifr).contents().find('#customizeDemo').attr('href', customizeurl);
         }
     }
     $.cookie('easyuiThemeName', themeName, {
    	  expires : 7, path:"/"
      });
 
 };
 if ($.cookie('easyuiThemeName')) {
     changeThemeFun($.cookie('easyuiThemeName'));
 }