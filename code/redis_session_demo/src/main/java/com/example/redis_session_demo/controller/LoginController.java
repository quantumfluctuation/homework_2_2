package com.example.redis_session_demo.controller;

import com.example.redis_session_demo.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @RequestMapping("/login")
    @ResponseBody
    public String login(HttpSession session, User user) throws Exception {
        if ("admin".equals(user.getName()) && "admin".equals(user.getPassword())) {
            session.setAttribute("user", user);
            return "success";
        }
        return "fail";
    }

}
