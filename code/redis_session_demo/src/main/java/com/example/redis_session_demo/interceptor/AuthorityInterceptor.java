package com.example.redis_session_demo.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthorityInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Object user = request.getSession().getAttribute("user");
        if (user == null) {
            //没有登录就重定向到登录页面
            response.sendRedirect("/redis_session_demo-0.0.1-SNAPSHOT/login.html");
            return false;
        }
        return true;
    }

}