package com.example.redis_session_demo.controller;

import com.example.redis_session_demo.dao.ResumeMapper;
import com.example.redis_session_demo.pojo.Resume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/resume")
public class ResumeController {

    @Autowired
    private ResumeMapper resumeMapper;

    @RequestMapping("/find")
    public Resume queryOne(Long id) throws Exception {
        return resumeMapper.findById(id);
    }

    @RequestMapping("/findAll")
    public List<Resume> queryAll() throws Exception {
        return resumeMapper.findAll();
    }

    @RequestMapping("/delete")
    public void delete(Long id) throws Exception {
        resumeMapper.deleteById(id);
    }

    @RequestMapping("/save")
    public void add(Resume resume) throws Exception {
        resumeMapper.save(resume);
    }

    @RequestMapping("/update")
    public void update(Resume resume) throws Exception {
        resumeMapper.update(resume);
    }

}
