package com.example.redis_session_demo.dao;


import com.example.redis_session_demo.pojo.Resume;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface ResumeMapper {

    Resume findById(Long id);

    List<Resume> findAll();

    void deleteById(Long id);

    void save(Resume resume);

    void update(Resume resume);
}
